﻿using AdminDatabases.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    public class PsqlRepository : IPSqlRepository
    {
        public void AddEntity(string table, object entity)
        {
            using mbm_dbContext db = new mbm_dbContext();
            if (table == "Categories")
            {
                db.Categories.Add(entity as Category);
            }
            else
            {
                db.CharacteristicValues.Add(entity as CharacteristicValue);
            }
            db.SaveChanges();
        }

        public void DeleteEntity(string table, int id)
        {
            using mbm_dbContext db = new mbm_dbContext();
            if (table == "Categories")
            {
                Category category = db.Categories.Where(c => c.Id == id).FirstOrDefault();
                db.Categories.Remove(category);
            }
            else
            {
                CharacteristicValue characteristicValue = db.CharacteristicValues.Where(c => c.Id == id).FirstOrDefault();
                db.CharacteristicValues.Remove(characteristicValue);
            }
            db.SaveChanges();
        }
        public IEnumerable<ProductCharacteristics> ProductCharacts()
        {
            using var db = new mbm_dbContext();
            var query = from productsValue in db.ProductsValues
                        join product in db.Products on productsValue.ProductId equals product.Id
                        join manufacturer in db.Manufacturers on product.ManufacturerId equals manufacturer.Id
                        join charactValues in db.CharacteristicValues on productsValue.CharId equals charactValues.Id
                        join charact in db.Characteristics on charactValues.CharId equals charact.Id
                        select new ProductCharacteristics()
                        {
                            ProductName = product.Name,
                            ManufacturerName = manufacturer.Name,
                            ProductModel = product.Model,
                            CharacteristicName = charact.Name,
                            CharacterisiticValue = charactValues.CharacteristicValue1,
                            Measument = charact.Measument
                        };
            return query.ToList();
        }
        public IEnumerable<ExistingProductWithCaracts> ExistingProduct()
        {
            using var db = new mbm_dbContext();
            var query = db.ProductsValues
            .Join(db.CharacteristicValues, productsValue => productsValue.CharId, charactValue => charactValue.Id,
            (productsValue, charactValue) =>
            new
            {
                CharId = charactValue.CharId,
                CharactValueId = charactValue.Id,
                CharactValue = charactValue.CharacteristicValue1
            })
            .Join(db.Characteristics, charactValue => charactValue.CharId, characts => characts.Id,
            (charactValue, characts) =>
            new
            {
                CharactValueId = charactValue.CharactValueId,
                CharactId = characts.Id,
                CharactValue = charactValue.CharactValue,
                CharactName = characts.Name
            })
            .GroupBy(q => new { q.CharactId, q.CharactValueId, q.CharactValue, q.CharactName })
            .Select(q => new ExistingProductWithCaracts
            {
                CharactName = q.Key.CharactName,
                CharacteristicsValue = q.Key.CharactValue,
                ProductCount = q.Count()
            })
            .OrderBy(q => q.CharactName)
            .ToList();
            return query;
        }
        public IEnumerable<ManufacturersProducts> ManufacturerProducts(string name)
        {
            using mbm_dbContext db = new mbm_dbContext();
            var query = db.Products
            .Select((products) =>
            new ManufacturersProducts
            {
                Name = products.Name,
                Model = products.Model,
                Manufacturer = products.Manufacturer
            })
            .Where(product => product.Manufacturer.Name == name)
            .ToList();
            return query;
        }
        public void UpdateEntity(int id, string tableName, object entity)
        {
            using mbm_dbContext db = new mbm_dbContext();
            if(tableName == "Categories")
            {
                Category newCategory = entity as Category;
                Category category = db.Categories.Where(c => c.Id == id).FirstOrDefault();
                category.Name = newCategory.Name;
                category.Description = newCategory.Description;
                category.Availability = newCategory.Availability;
                category.ImageUri = newCategory.ImageUri;
                db.Categories.Update(category);
                db.SaveChanges();
            }
            else
            {
                CharacteristicValue newCV = new();
                CharacteristicValue characteristicValue = db.CharacteristicValues.Where(c => c.Id == id).FirstOrDefault();
                characteristicValue.CharacteristicValue1 = newCV.CharacteristicValue1;
                characteristicValue.CharId = newCV.CharId;
                db.CharacteristicValues.Update(characteristicValue);
                db.SaveChanges();
            }
        }
    }
}
