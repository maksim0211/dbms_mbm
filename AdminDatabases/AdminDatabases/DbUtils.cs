﻿using Cassandra;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    public static class DbUtils
    {
        public static OracleConnection GetOracleConnection()
        {
            string host = "localhost";
            int port = 55202;
            string sid = "xe";
            string user = "mbm_user";
            string password = "mbm";


            string connString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = "
                 + host + ")(PORT = " + port + "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = "
                 + sid + ")));Password=" + password + ";User ID=" + user;


            OracleConnection conn = new OracleConnection();

            conn.ConnectionString = connString;

            return conn;
        }
        public static ISession GetCassandraConnection()
        {
            var session = Cluster.Builder()
                     .AddContactPoints("localhost")
                     .WithPort(55303)
                     .WithAuthProvider(new PlainTextAuthProvider("cassandra", "cassandra"))
                     .Build()
                     .Connect("mbm");
            return session;
        }
    }
}
