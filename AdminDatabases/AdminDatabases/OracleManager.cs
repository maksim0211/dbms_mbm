﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminDatabases
{
    class OracleManager : IChatManager
    {
        OracleRepository oracleRep = new OracleRepository();

        public void AddRowChat()
        {
            List<String> list = new List<string>();
            Console.WriteLine("Сейчас вы будете вводить данные для своей новой записи");
            list = oracleRep.getColumnsName("SELECT * FROM " + Program.TableName);

            string sqlInsert = "INSERT INTO " + Program.TableName;
            string sqlInsertTables = "(";
            string sqlInsertValues = "";

            foreach (var row in list)
            {
                Console.WriteLine("Введите значение для поля ");
                Console.Write(row+"\n");
                string value = Console.ReadLine();

                if (row != list[list.Count - 1])
                {
                    sqlInsertTables += row + ",";
                    sqlInsertValues += "'" + value + "',";
                }
                else
                {
                    sqlInsertTables += row + ")";
                    sqlInsertValues += "'" + value + "'";
                }
            }

            sqlInsert += sqlInsertTables + "VALUES (";
            sqlInsert += sqlInsertValues + ")";
            Console.WriteLine(sqlInsert);
            oracleRep.executeNonQuery(sqlInsert);
        }

        public void ReadAllChat()
        {
            List<String> list = new List<string>();
            list = oracleRep.getColumns("SELECT * FROM " + Program.TableName);
            int count = 1;

            foreach (var row in list)
            {
                Console.WriteLine("------------------" + count + "------------------");
                Console.WriteLine(row);
                count++;
            }
        }

        public void DeleteRowChat()
        {
            List<String> list = new List<string>();
            Console.WriteLine("Укажите номер поля по которому будет производиться поиск записи для УДАЛЕНИЯ:");
            list = oracleRep.getColumnsName("SELECT * FROM " + Program.TableName);

            int count = 0;

            foreach (var row in list)
            {
                Console.Write(count + "." + row+ "\n");
                count++;
            }

            Console.WriteLine();
            int number = Convert.ToInt32(Console.ReadLine());
            String findRowName = list[number];
            string findRowValueString;
            string finalFindRowValue = null;

            Console.WriteLine("Было выбранно поле: " + findRowName);
            Console.WriteLine("Укажите значение поля, по которому хотите найти запись: ");

            findRowValueString = Console.ReadLine();
            string sql = "SELECT * FROM " + Program.TableName + " WHERE " + findRowName + " = '" + findRowValueString + "'";
            finalFindRowValue = "'" + findRowValueString + "'";

            list = oracleRep.getColumns(sql);
            count = 1;

            foreach (var row in list)
            {
                Console.WriteLine("------------------" + count + "------------------");
                Console.WriteLine(row);
                count++;
            }

            if (count > 2)
            {
                Console.WriteLine("Было найдено более 2-ух записей по указанному запросу. УДАЛЕНИЕ может проводиться максимум по одной записи. Используйте другой параметр для поиска");
            }
            else if (count == 2)
            {
                sql = "DELETE FROM " + Program.TableName + " WHERE " + findRowName + " = " + finalFindRowValue;
                Console.WriteLine(sql);
                oracleRep.executeNonQuery(sql);
            }
        }

        public void UpdateRowChat()
        {
            List<String> list = new List<string>();
            List<String> list2 = new List<string>();
            Console.WriteLine("Укажите номер поля по которому будет производиться поиск записи для изменения:");
            list = oracleRep.getColumnsName("SELECT * FROM " + Program.TableName);

            int count = 0;

            foreach (var row in list)
            {
                Console.Write(count + "." + row + "\n");
                count++;
            }

            Console.WriteLine();
            int number = Convert.ToInt32(Console.ReadLine());
            String findRowName = list[number];
            string findRowValueString;
            string finalFindRowValue = null;

            Console.WriteLine("Было выбранно поле: " + findRowName);
            Console.WriteLine("Укажите значение поля, по которому хотите найти запись: ");

            findRowValueString = Console.ReadLine();
            string sql = "SELECT * FROM " + Program.TableName + " WHERE " + findRowName + " = '" + findRowValueString + "'";
            finalFindRowValue = "'" + findRowValueString + "'";

            list2 = oracleRep.getColumns(sql);
            count = 1;

            foreach (var row in list2)
            {
                Console.WriteLine("------------------" + count + "------------------");
                Console.WriteLine(row);
                count++;
            }

            if (count > 2)
            {
                Console.WriteLine("Было найдено более 2-ух записей по указанному запросу. Обновление может проводиться максимум по одной записи. Используйте другой параметр для поиска");
            }
            else if (count == 2)
            {
                Console.WriteLine("Укажите номер поля, которое вы будете изменять: ");

                count = 0;

                foreach (var row in list)
                {
                    Console.Write(count + "." + row + "\n");
                    count++;
                }

                Console.WriteLine();
                number = Convert.ToInt32(Console.ReadLine());
                String changeRowName = list[number];
                string changeRowValueString;

                Console.WriteLine("Было выбранно поле: " + changeRowName);
                Console.WriteLine("Укажите новое значение для указанного поля: ");

                changeRowValueString = Console.ReadLine();
                sql = "UPDATE " + Program.TableName + " SET " + changeRowName + " = '" + changeRowValueString + "' WHERE " + findRowName + " = " + finalFindRowValue;
                Console.WriteLine(sql);
                oracleRep.executeNonQuery(sql);
            }
        }

        public void Query1Chat()
        {
            String sql = "SELECT products.name, manufacturers.name, products.model,characteristics.name, characteristic_values.characteristic_value, CHARACTERISTICS.MEASUMENT FROM products_values JOIN products ON product_id = products.id JOIN manufacturers ON manufacturers.id = products.manufacturer_id JOIN characteristic_values ON characteristic_values.id = products_values.value_id JOIN characteristics ON characteristics.id = characteristic_values.char_id";
            List<String> list = new List<string>();
            list = oracleRep.getColumns(sql);
            int count = 1;

            foreach (var row in list)
            {
                Console.WriteLine("------------------" + count + "------------------");
                Console.WriteLine(row);
                count++;
            }
        }

        public void Query2Chat()
        {
            String sql = "SELECT * FROM products JOIN manufacturers ON manufacturers.id = products.manufacturer_id WHERE manufacturers.id = 1";
            List<String> list = new List<string>();
            list = oracleRep.getColumns(sql);
            int count = 1;

            foreach (var row in list)
            {
                Console.WriteLine("------------------" + count + "------------------");
                Console.WriteLine(row);
                count++;
            }
        }

        public void Query3Chat()
        {
            String sql = "SELECT ORDERS.ID,ORDERS.ADDRESS,ORDERS.PAY_TYPE,ORDERS.STATUS,CLIENTS.PHONE,CLIENTS.MAIL FROM ORDERS JOIN CLIENTS ON orders.client_id = clients.id";
            List<String> list = new List<string>();
            list = oracleRep.getColumns(sql);
            int count = 1;

            foreach (var row in list)
            {
                Console.WriteLine("------------------" + count + "------------------");
                Console.WriteLine(row);
                count++;
            }
        }

        public void Query4Chat()
        {
            Console.WriteLine("Запрос возможен только для NoSQL СУБД");
        }

        public void Query5Chat()
        {
            Console.WriteLine("Запрос возможен только для NoSQL СУБД");
        }

        public List<string> GetAcceptedTables()
        {
            List<string> acceptedTables = new List<string>();
            acceptedTables.Add("categories");
            acceptedTables.Add("characteristic_values");
            acceptedTables.Add("characteristics");
            acceptedTables.Add("clients");
            acceptedTables.Add("deliveries");
            acceptedTables.Add("manufacturers");
            acceptedTables.Add("orders");
            acceptedTables.Add("product_photos");
            acceptedTables.Add("products");
            acceptedTables.Add("products_orders");
            acceptedTables.Add("products_values");
            acceptedTables.Add("reviews");

            return acceptedTables;
        }
    }
}
