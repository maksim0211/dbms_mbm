﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra;

namespace AdminDatabases
{
    interface ICassandraRepository
    {
        RowSet getColumnsAndTypes();

        RowSet getColumns();

        RowSet getTableByColumns(string colnames);

        RowSet getMaxValue();

        RowSet getQuery1();

        RowSet getQuery2(string producername);

        RowSet getQuery3(string clientlogin);

        RowSet getQuery4(string clientlogin);

        RowSet getQuery5(string model);

        void InsertQ(string colnames, string values);

        void DeleteQ(int numrow);

        void UpdateQ(string id, string login,string colname, string newval);

    }
}
