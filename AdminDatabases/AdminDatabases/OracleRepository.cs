﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    class OracleRepository : IOracleRepository
    {
        public List<String> getColumns(string sql1)
        {
            string sql = sql1;

            OracleConnection oracleConnection = DbUtils.GetOracleConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = oracleConnection;
            cmd.CommandText = sql;

            oracleConnection.Open();
            OracleDataReader reader = cmd.ExecuteReader();

            List<String> list = new List<String>();
            int cols = reader.FieldCount;
            String tmp = "";

            while (reader.Read())
            {
                for (int i = 0; i < cols; i++)
                {
                    tmp += (reader.GetName(i) + ": " + reader.GetValue(i)+"\n");
                }
                list.Add(tmp);
                tmp = "";
            }

            oracleConnection.Close();
            return list;
        }

        public List<String> getColumnsName(string sql1)
        {
            string sql = sql1;

            OracleConnection oracleConnection = DbUtils.GetOracleConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = oracleConnection;
            cmd.CommandText = sql;

            oracleConnection.Open();
            OracleDataReader reader = cmd.ExecuteReader();

            List<String> list = new List<String>();
            int cols = reader.FieldCount;
            String tmp = "";

            int count = 0;

            while (reader.Read())
            {
                if (count == 0)
                {
                    for (int i = 0; i < cols; i++)
                    {
                        list.Add(reader.GetName(i));
                    }
                    count = 1;
                }
            }

            oracleConnection.Close();
            return list;
        }

        public void executeNonQuery(String sqlString)
        {
            // add (insert), delete
            OracleConnection oracleConnection = DbUtils.GetOracleConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = oracleConnection;
            cmd.CommandText = sqlString;

            oracleConnection.Open();
            cmd.ExecuteNonQuery();

            oracleConnection.Close();
        }
    }
}
