﻿using AdminDatabases.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    interface IPSqlRepository
    {
        public void AddEntity(string table, object entity);
        public void DeleteEntity(string table, int id);
        public IEnumerable<ProductCharacteristics> ProductCharacts();
        public IEnumerable<ExistingProductWithCaracts> ExistingProduct();
        public IEnumerable<ManufacturersProducts> ManufacturerProducts(string name);
        public void UpdateEntity(int id, string tableName, object entity);

    }
}
