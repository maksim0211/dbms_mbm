﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class Order
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string PayType { get; set; }
        public string Status { get; set; }
        public short Count { get; set; }
        public int ClientId { get; set; }

        public virtual Client Client { get; set; }
        public virtual Delivery Delivery { get; set; }
    }
}
