﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class Product
    {
        public Product()
        {
            ProductPhotos = new HashSet<ProductPhoto>();
            Reviews = new HashSet<Review>();
        }

        public int Id { get; set; }
        public short Count { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Cost { get; set; }
        public int CategoryId { get; set; }
        public int ManufacturerId { get; set; }
        public string Model { get; set; }

        public virtual Category Category { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual ICollection<ProductPhoto> ProductPhotos { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
