﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Availability { get; set; }
        public string ImageUri { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
