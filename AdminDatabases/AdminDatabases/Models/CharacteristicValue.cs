﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class CharacteristicValue
    {
        public int Id { get; set; }
        public string CharacteristicValue1 { get; set; }
        public int CharId { get; set; }

        public virtual Characteristic Char { get; set; }
    }
}
