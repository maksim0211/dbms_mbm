﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class ProductsValue
    {
        public int ProductId { get; set; }
        public int CharId { get; set; }

        public virtual CharacteristicValue Char { get; set; }
        public virtual Product Product { get; set; }
    }
}
