﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class Characteristic
    {
        public Characteristic()
        {
            CharacteristicValues = new HashSet<CharacteristicValue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUri { get; set; }
        public string Measument { get; set; }

        public virtual ICollection<CharacteristicValue> CharacteristicValues { get; set; }
    }
}
