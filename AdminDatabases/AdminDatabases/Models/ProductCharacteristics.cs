﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases.Models
{
    public class ProductCharacteristics
    {
        public string ProductName { get; set; }
        public string ManufacturerName { get; set; }
        public string ProductModel { get; set; }
        public string CharacteristicName { get; set; }
        public string CharacterisiticValue { get; set; }
        public string Measument { get; set; }
    }
}
