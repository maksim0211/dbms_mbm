﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases.Models
{
    public class ExistingProductWithCaracts
    {
        public string CharactName { get; set; }
        public string CharacteristicsValue { get; set; }
        public int ProductCount { get; set; }
    }
}
