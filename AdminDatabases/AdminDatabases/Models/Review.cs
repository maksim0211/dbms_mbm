﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class Review
    {
        public int Id { get; set; }
        public short Score { get; set; }
        public string Comment { get; set; }
        public int ClientId { get; set; }
        public int ProductId { get; set; }
        public virtual Client Client { get; set; }
        public virtual Product Product { get; set; }
    }
}
