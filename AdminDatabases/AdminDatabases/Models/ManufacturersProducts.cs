﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases.Models
{
    public class ManufacturersProducts
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public Manufacturer Manufacturer { get; set; }
    }
}
