﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdminDatabases
{
    public partial class ProductPhoto
    {
        public int Id { get; set; }
        public string PhotoUri { get; set; }
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
