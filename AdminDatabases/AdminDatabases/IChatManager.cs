﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    public interface IChatManager
    {
        List<String> GetAcceptedTables();
        void ReadAllChat();
        void DeleteRowChat();
        void AddRowChat();
        void UpdateRowChat();
        void Query1Chat();
        void Query2Chat();
        void Query3Chat();
        void Query4Chat();
        void Query5Chat();
    }
}
