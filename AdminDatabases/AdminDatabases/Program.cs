﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AdminDatabases
{
    class Program
    {
        public static string TableName = "";
        public static IChatManager ChatManager = null;
        static void Main(string[] args)
        {
            ChooseDatabase();
            WorkWithDatabase();
        }
        static void WorkWithDatabase()
        {
            bool exit = false;
            while (!exit)
            {
                Queries.DisplayMainMenu(ChatManager);
                TableName = Console.ReadLine();
                List<string> acceptedTables = ChatManager.GetAcceptedTables();
                if (!acceptedTables.Contains(TableName.Trim().ToLower()))
                {
                    Console.WriteLine("Retry");
                    continue;
                }
                exit = WorkWithTable();

            }
        }
        static bool WorkWithTable()
        {
            bool exit = false;
            while (true)
            {
                Queries.DisplayToolsMenu();
                int queryNumber = int.Parse(Console.ReadLine());
                int result = Queries.OperationHandler(queryNumber, TableName, ChatManager);
                if (result == Queries.ChangeDatabase)
                {
                    ChooseDatabase();
                    break;
                }
                if (result == Queries.ChangeTable)
                    break;
                if (result == Queries.Exit)
                {
                    exit = true;
                    break;
                }
            }
            return exit;
        }
        static void ChooseDatabase()
        {
            bool retry = true;
            Console.WriteLine("Oracle - 1\nCassandra - 2\nPSQL - 3");
            while (retry)
            {
                int databaseType = int.Parse(Console.ReadLine());
                if (databaseType > 0 && databaseType < 4)
                {
                    retry = false;
                    switch (databaseType)
                    {
                        case 1:
                            ChatManager = new OracleManager();
                            break;
                        case 2:
                            ChatManager = new CassandraManager();
                            break;
                        case 3:
                            ChatManager = new PsqlManager();
                            break;
                    }
                }
                else
                    Console.WriteLine("Retry");
            }
        }
    }
}
