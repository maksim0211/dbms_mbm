﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdminDatabases
{
    class CassandraManager : IChatManager
    {
        CassandraRepository casRep = new CassandraRepository();
        public void AddRowChat()
        {
            List<string> listColNames = new List<string>();
            List<string> listColTypes = new List<string>();
            List<object> listInputCol = new List<object>();

            Console.WriteLine("Возможные к заполнению столбцы:");
            Console.WriteLine("<colname>,<type>");
            foreach (var col in casRep.getColumnsAndTypes())
            {
                listColNames.Add(col.GetValue<string>(0));
                listColTypes.Add(col.GetValue<string>(1));
                Console.WriteLine(listColNames[listColNames.Count - 1] + ", " + listColTypes[listColTypes.Count - 1]);
            }

            Console.WriteLine("--------------------------------------");
            Console.WriteLine("Вводите данные с учетом их типа, если тип = text, вводите значение в кавычках: 'добавляемое_значение'");
            Console.WriteLine("если тип = list<text>, вводите значение следующим образом: <['textval1','textval2']>, квадратные скобки и кавычки обязательны");
            Console.WriteLine("если тип = list<int>, вводите значение следующим образом: <[intval1, intval2]>, квадратные скобки обязательны");

			int maxvalue = -1;
			foreach (var col in casRep.getMaxValue())
			{
				maxvalue = col.GetValue<int>(0);
			}
			Console.WriteLine("Подсказка : максимальное значение для id_" + Program.TableName + " = " + maxvalue);
			Console.WriteLine("--------------------------------------");

			for (int i = 0; i < listColNames.Count; i++)
			{
				Console.WriteLine("Введите данные для следующего столбца: " + listColNames[i] + ", " + listColTypes[i]);
				listInputCol.Add(Console.ReadLine());
			}
			String strInputColValue = "";
			String strInputColName = "";
			for (int i = 0; i < listColNames.Count; i++)
			{
				strInputColName += listColNames[i];
				if (listInputCol[i].ToString() == "")
					strInputColValue += "null";
				else
					strInputColValue += listInputCol[i];
				if (i + 1 != listColNames.Count)
				{
					strInputColValue += ",";
					strInputColName += ",";
				}
			}
			casRep.InsertQ(strInputColName, strInputColValue);
			Console.WriteLine("Строка добавлена успешно!");
		}

		public void ReadAllChat()
        {
			List<string> listColNames = new List<string>();
			String colNames = "";
			foreach (var col in casRep.getColumns())
			{
				listColNames.Add(col.GetValue<string>(0));
				colNames += col.GetValue<string>(0);
				colNames += " ,";
			}
			colNames = colNames.Substring(0, colNames.Length - 1);

			var rs = casRep.getTableByColumns(colNames);
			var colCount = rs.Columns.Count();
			var rowCounter = 0;

			foreach (var row in rs)
			{
				rowCounter += 1;
				Console.WriteLine("Row #" + rowCounter);
				Console.WriteLine("------------------------------------------------------------------");
				for (int i = 0; i < colCount; i++)
				{
					if (row.GetValue<object>(i) != null)
						if (row.GetValue<object>(i).GetType().ToString() == "System.Int32[]")
							Console.WriteLine(listColNames[i] + ": " + GetValueFromInt32Arr(row.GetValue<System.Int32[]>(i)));
						else if (row.GetValue<object>(i).GetType().ToString() == "System.String[]")
							Console.WriteLine(listColNames[i] + ": " + GetValueFromStringArr(row.GetValue<System.String[]>(i)));
						else
							Console.WriteLine(listColNames[i] + ": " + row.GetValue<object>(i));
				}

				Console.WriteLine("------------------------------------------------------------------");
			}
		}

		public void DeleteRowChat()
        {
			ReadAllChat();
			Console.WriteLine("Какую строчку удалить? (введите значение id_" + Program.TableName + ")");
			int numRow = Convert.ToInt32(Console.ReadLine());
			casRep.DeleteQ(numRow);
			Console.WriteLine("Успешно!");
		}

		public void UpdateRowChat()
        {
			ReadAllChat();
			Console.WriteLine("Строку с каким id_" + Program.TableName + " необходимо обновить?");
			var id = Console.ReadLine();
			Console.WriteLine("Если в таблице присутствует поле login, являющееся частью композитного PK, также впишите его (в кавычках), иначе нажмите Enter");
			var login = Console.ReadLine();
			Console.WriteLine("Название изменяемого столбца?");
			var colname = Console.ReadLine();
			Console.WriteLine("Новое значение? (если текст, то снова в кавычках)");
			var newval = Console.ReadLine();
			casRep.UpdateQ(id,login,colname,newval);
			Console.WriteLine("Изменение успешно!");
		}

		public void Query1Chat()
        {
			List<string> listColNames = new List<string>() { "id_product", "name", "model", "producer_name", "ch_proc_type", "ch_screen_diagonal", "ch_sim_count", "ch_type_storage_device",
				"ch_vol_built_in_memory", "ch_vol_memory", "ch_vol_ram", "ch_vol_videomemory"};
			var colCount = listColNames.Count;
			var rowCounter = 0;
			foreach (var row in casRep.getQuery1())
			{
				rowCounter += 1;
				Console.WriteLine("Row #" + rowCounter);
				Console.WriteLine("------------------------------------------------------------------");
				for (int i = 0; i < colCount; i++)
				{
					if (row.GetValue<object>(i) == null)
						Console.WriteLine(listColNames[i] + ": -");
					else if (row.GetValue<object>(i).GetType().ToString() == "System.Int32[]")
						Console.WriteLine(listColNames[i] + ": " + GetValueFromInt32Arr(row.GetValue<System.Int32[]>(i)));
					else if (row.GetValue<object>(i).GetType().ToString() == "System.String[]")
						Console.WriteLine(listColNames[i] + ": " + GetValueFromStringArr(row.GetValue<System.String[]>(i)));
					else
						Console.WriteLine(listColNames[i] + ": " + row.GetValue<object>(i));
				}

				Console.WriteLine("------------------------------------------------------------------");
			}
			listColNames.Clear();
		}

		public void Query2Chat()
        {
			Console.WriteLine("Введите название производителя в кавычках, продукты которого необходимо показать: ");
			var input_producer_name = Console.ReadLine();
			if (input_producer_name.Contains("--") || input_producer_name.Contains("//"))
			{
				throw new Exception("SQL INJECTION!!!");
				System.Environment.Exit(187);
			}
			List<string> listColNames = new List<string>() { "id_product", "name", "model", "producer_name", "ch_proc_type", "ch_screen_diagonal", "ch_sim_count", "ch_type_storage_device",
				"ch_vol_built_in_memory", "ch_vol_memory", "ch_vol_ram", "ch_vol_videomemory"};
			var colCount = listColNames.Count;
			var rowCounter = 0;

			foreach (var row in casRep.getQuery2(input_producer_name))
			{
				rowCounter += 1;
				Console.WriteLine("Row #" + rowCounter);
				Console.WriteLine("------------------------------------------------------------------");
				for (int i = 0; i < colCount; i++)
				{
					if (row.GetValue<object>(i) == null)
						Console.WriteLine(listColNames[i] + ": -");
					else if (row.GetValue<object>(i).GetType().ToString() == "System.Int32[]")
						Console.WriteLine(listColNames[i] + ": " + GetValueFromInt32Arr(row.GetValue<System.Int32[]>(i)));
					else if (row.GetValue<object>(i).GetType().ToString() == "System.String[]")
						Console.WriteLine(listColNames[i] + ": " + GetValueFromStringArr(row.GetValue<System.String[]>(i)));
					else
						Console.WriteLine(listColNames[i] + ": " + row.GetValue<object>(i));
				}

				Console.WriteLine("------------------------------------------------------------------");
			}
			listColNames.Clear();
		}

		public void Query3Chat()
        {
			Console.WriteLine("Введите логин клиента в кавычках, заказы которого необходимо показать: ");
			var input_client_login = Console.ReadLine();
			if (input_client_login.Contains("--") || input_client_login.Contains("//"))
			{
				throw new Exception("SQL INJECTION!!!");
				System.Environment.Exit(187);
			}
			List<string> listColNames = new List<string>();
			String colNames = "";
			foreach (var col in casRep.getColumns())
			{
				listColNames.Add(col.GetValue<string>(0));
				colNames += col.GetValue<string>(0);
				colNames += " ,";
			}
			colNames = colNames.Substring(0, colNames.Length - 1);
			var colCount = listColNames.Count;
			var rowCounter = 0;

			foreach (var row in casRep.getQuery3(input_client_login))
			{
				rowCounter += 1;
				Console.WriteLine("Row #" + rowCounter);
				Console.WriteLine("------------------------------------------------------------------");
				for (int i = 0; i < colCount; i++)
				{
					if (row.GetValue<object>(i) == null)
						Console.WriteLine(listColNames[i] + ": -");
					else if (row.GetValue<object>(i).GetType().ToString() == "System.Int32[]")
						Console.WriteLine(listColNames[i] + ": " + GetValueFromInt32Arr(row.GetValue<System.Int32[]>(i)));
					else if (row.GetValue<object>(i).GetType().ToString() == "System.String[]")
						Console.WriteLine(listColNames[i] + ": " + GetValueFromStringArr(row.GetValue<System.String[]>(i)));
					else
						Console.WriteLine(listColNames[i] + ": " + row.GetValue<object>(i));
				}

				Console.WriteLine("------------------------------------------------------------------");
			}
			listColNames.Clear();
		}

		public void Query4Chat()
		{
			var session = DbUtils.GetCassandraConnection();
			Console.WriteLine("Введите логин клиента в кавычках, количество сделанных заказов которого надо показать");
			var input_client_login = Console.ReadLine();
			if (input_client_login.Contains("--") || input_client_login.Contains("//"))
			{
				throw new Exception("SQL INJECTION!!!");
				System.Environment.Exit(187);
			}
			foreach (var row in casRep.getQuery4(input_client_login))
			{
				Console.WriteLine("Count: " + row.GetValue<object>(0));
			}
			Console.WriteLine("------------------------------------------------------------------");

			session.Dispose();
			session = null;
		}

		public void Query5Chat() {
			var session = DbUtils.GetCassandraConnection();
			Console.WriteLine("Введите модель товара в кавчках, среднюю округленную оценку которого необходимо показать");
			var input_client_model = Console.ReadLine();
			if (input_client_model.Contains("--") || input_client_model.Contains("//"))
			{
				throw new Exception("SQL INJECTION!!!");
				System.Environment.Exit(187);
			}
			foreach (var row in casRep.getQuery5(input_client_model))
			{
				Console.WriteLine("Average rate: " + row.GetValue<object>(0));
			}
			Console.WriteLine("------------------------------------------------------------------");

			session.Dispose();
			session = null;
		}

		public string GetValueFromInt32Arr(System.Int32[] arr)
		{
			string res = "";
			for (int i = 0; i < arr.Length; i++)
			{
				res += arr[i];
				res += ",";
			}
			return res.Substring(0, res.Length - 1);
		}

		public string GetValueFromStringArr(System.String[] arr)
		{
			string res = "";
			for (int i = 0; i < arr.Length; i++)
			{
				res += arr[i];
				res += ",";
			}
			return res.Substring(0, res.Length - 1);
		}

        public List<string> GetAcceptedTables()
        {
			List<string> acceptedTables = new List<string>() { "product", "basket", "order", "review", "person" };
			return acceptedTables;
		}
    }
}
