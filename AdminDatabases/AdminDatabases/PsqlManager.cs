﻿using AdminDatabases.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    //Не вижу смысла создавать класс PsqlRepository, т.к. вадим работает здесь через сущности.
    class PsqlManager : IChatManager
    {
        private PsqlRepository psql = new PsqlRepository();

        void IChatManager.AddRowChat()
        {
            string tableName = Program.TableName;

            if (tableName == "Categories")
            {
                Console.WriteLine("Введите имя, описание, наличие и uri картинки построчно");
                string name = Console.ReadLine();
                string description = Console.ReadLine();
                bool availability = bool.Parse(Console.ReadLine());
                string imageUri = Console.ReadLine();
                Category category = new Category() { Name = name, Description = description, Availability = availability, ImageUri = imageUri };
                psql.AddEntity(tableName, category);
            }
            else
            {
                Console.WriteLine("Введите значение характеристики и ID характеристики построчно");
                string value = Console.ReadLine();
                int id = int.Parse(Console.ReadLine());
                CharacteristicValue cv = new() { CharacteristicValue1 = value, CharId = id };
                psql.AddEntity(tableName, cv);
            }

        }

        void IChatManager.DeleteRowChat()
        {
            string tableName = Program.TableName;
            using mbm_dbContext db = new mbm_dbContext();
            Console.WriteLine("Введите id для удаления");
            int id = int.Parse(Console.ReadLine());
            psql.DeleteEntity(tableName, id);
        }

        List<string> IChatManager.GetAcceptedTables()
        {
            return new List<string>() { "categories", "characteristicvalues" };
        }

        void IChatManager.Query1Chat()
        {
            var query = psql.ProductCharacts();
            foreach (var q in query)
                Console.WriteLine($"{q.ProductName}; {q.ManufacturerName}; {q.ProductModel}; {q.CharacteristicName}; {q.CharacterisiticValue}; {q.Measument}");
        }

        void IChatManager.Query2Chat()
        {
            var query = psql.ExistingProduct();
            foreach (var q in query)
                Console.WriteLine($"{q.CharactName}; {q.CharacteristicsValue}; {q.ProductCount}");
        }

        void IChatManager.Query3Chat()
        {
            Console.WriteLine("Введите название производителя");
            string name = Console.ReadLine();
            var query = psql.ManufacturerProducts(name);
            foreach (var q in query)
                Console.WriteLine($"{q.Name}; {q.Model}; {q.Manufacturer.Name}");
        }

        void IChatManager.Query4Chat()
        {
            Console.WriteLine("Запрос возможен только для NoSQL СУБД");
        }

        void IChatManager.Query5Chat()
        {
            Console.WriteLine("Запрос возможен только для NoSQL СУБД");
        }

        void IChatManager.ReadAllChat()
        {
            string tableName = Program.TableName;
            using mbm_dbContext db = new mbm_dbContext();
            if (tableName == "Categories")
            {
                var categories = db.Categories.ToList();
                foreach (Category c in categories)
                    Console.WriteLine($"{c.Id} - {c.Name} - {c.Description} - {c.Availability} - {c.ImageUri}");
            }
            else
            {
                var values = db.CharacteristicValues.ToList();
                foreach (CharacteristicValue c in values)
                    Console.WriteLine($"{c.Id} - {c.CharacteristicValue1} - {c.CharId}");
            }
        }
        void IChatManager.UpdateRowChat()
        {

            string tableName = Program.TableName;

            if (tableName == "Categories")
            {
                Category category = new Category();
                Console.WriteLine("Введите id для обновления");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите имя, описание, наличие и uri картинки построчно");
                category.Name = Console.ReadLine();
                category.Description = Console.ReadLine();
                category.Availability = bool.Parse(Console.ReadLine());
                category.ImageUri = Console.ReadLine();
                psql.UpdateEntity(id, tableName, category);
            }
            else
            {
                Console.WriteLine("Введите id для удаления");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите значение характеристики и ID характеристики построчно");
                CharacteristicValue cv = new CharacteristicValue();
                cv.CharacteristicValue1 = Console.ReadLine();
                cv.CharId = int.Parse(Console.ReadLine());
                psql.UpdateEntity(id, tableName, cv);
            }
        }
    }
}
