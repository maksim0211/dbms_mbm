﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    interface IOracleRepository
    {
        List<String> getColumns(string sql1);

        List<String> getColumnsName(string sql1);

        public void executeNonQuery(String sqlString);
    }
}
