﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDatabases
{
    class Queries
    {
        public static readonly int ChangeDatabase = 0;
        public static readonly int ChangeTable = 1;
        public static readonly int Exit = 2;

        public static int OperationHandler(int switchNumber, String tableName, IChatManager chatManager)
        {
            int result = -1;

            switch (switchNumber)
            {
                case 0:
                    chatManager.ReadAllChat();
                    break;
                case 1:
                    chatManager.DeleteRowChat();
                    break;
                case 2:
                    chatManager.UpdateRowChat();
                    break;
                case 3:
                    chatManager.AddRowChat();
                    break;
                case 4:
                    chatManager.Query1Chat();
                    break;
                case 5:
                    chatManager.Query2Chat();
                    break;
                case 6:
                    chatManager.Query3Chat();
                    break;
                case 7:
                    chatManager.Query4Chat();
                    break;
                case 11:
                    chatManager.Query5Chat();
                    break;
                case 8:
                    result = ChangeDatabase;
                    break;
                case 9:
                    result = ChangeTable;
                    break;
                case 10:
                    Console.WriteLine("Спасибо за использование нашей программы, всего доброго");
                    result = Exit;
                    break;
                default:
                    Console.WriteLine("Введите число в промежутке 0-3 или 8 - для смены базы данных, 9 для выбора другой таблицы или 10 для завершения работы");
                    break;
            }
            return result;
        }
        public static void DisplayMainMenu(IChatManager chatManager)
        {
            Console.WriteLine("------------------------------ DATABASE MAIN MENU -----------------------------------");
            Console.WriteLine("Введите название таблицы, с которой хотите работать: ");

            if (chatManager is CassandraManager)
            {
                Console.WriteLine("Выберите: person, review, order, product, basket");
            }
            
            else if(chatManager is OracleManager)
            {
                Console.WriteLine("Выберите: CATEGORIES, CHARACTERISTIC_VALUES, CHARACTERISTICS, CLIENTS, DELIVERIES, MANUFACTURERS, ORDERS, PRODUCT_PHOTOS, PRODUCTS, PRODUCTS_ORDERS, PRODUCTS_VALUES, REVIEWS".ToLower());
            }
            
            else if(chatManager is PsqlManager)
            {
                Console.WriteLine("Выберите: \n1. Categories\n2. CharacteristicValues");
            }
            
        }

        public static void DisplayToolsMenu()
        {
            Console.WriteLine("------------------------------ DATABASE TOOLS MENU ----------------------------------");
            Console.WriteLine("Выберите что вы хотите сделать:");
            Console.WriteLine("0. Просмотреть таблицу ");
            Console.WriteLine("1. Удалить запись в таблице ");
            Console.WriteLine("2. Изменить значение записи в таблице");
            Console.WriteLine("3. Добавить запись в таблицу\n");

            Console.WriteLine("4. Вывести первый запрос");
            Console.WriteLine("5. Вывести второй запрос");
            Console.WriteLine("6. Вывести третий запрос\n");
            Console.WriteLine("7. Вывод функционального запроса(NoSQL)1\n");
            Console.WriteLine("11. Вывод функционального запроса(NoSQL)2\n");

            Console.WriteLine("8. Выбрать другую базу данных");
            Console.WriteLine("9. Выбрать другую таблицу");
            Console.WriteLine("10. Выйти из программы");
            Console.WriteLine("------------------------------------------------------------------------------------");
        }
    }
}
