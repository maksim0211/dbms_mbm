﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra;

namespace AdminDatabases
{
    class CassandraRepository : ICassandraRepository
    {
        public void DeleteQ(int numrow)
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("DELETE FROM mbm." + Program.TableName + " WHERE id_" + Program.TableName + " = " + numrow + ";");
            session.Dispose();
        }

        public RowSet getColumns()
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT column_name FROM system_schema.columns WHERE keyspace_name = 'mbm' AND table_name = '" + Program.TableName + "';");
            session.Dispose();
            return rs;
        }

        public RowSet getColumnsAndTypes()
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT column_name,type FROM system_schema.columns WHERE keyspace_name = 'mbm' AND table_name = '" + Program.TableName + "';");
            session.Dispose();
            return rs;
        }

        public RowSet getMaxValue()
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT max(id_" + Program.TableName + ") FROM " + Program.TableName);
            session.Dispose();
            return rs;
        }

        public RowSet getQuery1()
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT id_product, name, model, producer_name, ch_proc_type , ch_screen_diagonal, " +
                "ch_sim_count, ch_type_storage_device, ch_vol_built_in_memory, ch_vol_memory, ch_vol_ram,  ch_vol_videomemory" +
                " FROM product");
            session.Dispose();
            return rs;
        }

        public RowSet getQuery2(string producername)
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT id_product, name, model, producer_name, ch_proc_type , ch_screen_diagonal, " +
                "ch_sim_count, ch_type_storage_device, ch_vol_built_in_memory, ch_vol_memory, ch_vol_ram,  ch_vol_videomemory" +
                " FROM product WHERE producer_name = " + producername + " ALLOW FILTERING");
            session.Dispose();
            return rs;
        }

        public RowSet getQuery3(string clientlogin)
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT * FROM \"order\" WHERE login = " + clientlogin + " ALLOW FILTERING");
            session.Dispose();
            return rs;
        }

        public RowSet getQuery4(string clientlogin)
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT count(*) FROM mbm.\"order\" WHERE login = " + clientlogin + " ALLOW FILTERING;");
            session.Dispose();
            return rs;
        }

        public RowSet getQuery5(string model)
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT avg(rate) from review where product_model = " + model + " allow filtering ");
            session.Dispose();
            return rs;
        }

        public RowSet getTableByColumns(string colnames)
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = session.Execute("SELECT " + colnames + " FROM \"" + Program.TableName + "\"");
            session.Dispose();
            return rs;
        }

        public void InsertQ(string colnames, string values)
        {
            var session = DbUtils.GetCassandraConnection();
            session.Execute("INSERT INTO mbm." + Program.TableName + " (" + colnames + ") VALUES (" + values + ");");
            session.Dispose();
        }

        public void UpdateQ(string id, string login, string colname, string newval)
        {
            var session = DbUtils.GetCassandraConnection();
            var rs = new RowSet();
            if (login.Length > 0)
                rs = session.Execute("update " + Program.TableName + " set " + colname + " = " + newval + " where id_" + Program.TableName + " = " + id + " and login = " + login);
            else
            {
                rs = session.Execute("update " + Program.TableName + " set " + colname + " = " + newval + " where id_" + Program.TableName + " = " + id);
            }
            session.Dispose();
        }
    }
}
