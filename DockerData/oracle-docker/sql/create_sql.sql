CREATE USER mbm_user
  IDENTIFIED BY mbm;

GRANT create session TO mbm_user;
GRANT create table TO mbm_user;
GRANT create view TO mbm_user;
GRANT create any trigger TO mbm_user;
GRANT create any procedure TO mbm_user;
GRANT create sequence TO mbm_user;
GRANT create synonym TO mbm_user;

ALTER USER mbm_user quota unlimited on SYSTEM;

GRANT UNLIMITED TABLESPACE TO mbm_user;

CREATE TABLE mbm_user.characteristics(
id number PRIMARY KEY,
name VARCHAR(50) NOT NULL,
description VARCHAR(200) NOT NULL,
image_uri VARCHAR(100) NOT NULL,
measument VARCHAR(20) NOT NULL);

CREATE TABLE mbm_user.characteristic_values(
id number PRIMARY KEY,
characteristic_value VARCHAR(20) NOT NULL,
char_id INT NOT NULL,
FOREIGN KEY(char_id) REFERENCES mbm_user.characteristics(id));

CREATE TABLE mbm_user.manufacturers(
id number PRIMARY KEY,
country VARCHAR(40) NOT NULL,
name VARCHAR(50) NOT NULL,
description VARCHAR(200) NOT NULL,
logo_uri VARCHAR(100) NOT NULL);

CREATE TABLE mbm_user.categories(
id number PRIMARY KEY,
name VARCHAR(40) NOT NULL,
description VARCHAR(100) NOT NULL,
availability NUMBER(1) DEFAULT 0 NOT NULL,
image_uri VARCHAR(100) NOT NULL);

CREATE TABLE mbm_user.products(
id number PRIMARY KEY,
count number NOT NULL,
name VARCHAR(40) NOT NULL,
description VARCHAR(1000) NOT NULL,
cost number NOT NULL,
availability NUMBER(1) DEFAULT 0 NOT NULL,
category_id number NOT NULL,
manufacturer_id number NOT NULL,
FOREIGN KEY(category_id) REFERENCES mbm_user.categories(id),
FOREIGN KEY(manufacturer_id) REFERENCES mbm_user.manufacturers(id));

CREATE TABLE mbm_user.clients(
id number PRIMARY KEY,
login VARCHAR(40) NOT NULL,
password VARCHAR(40) NOT NULL,
status VARCHAR(20) NOT NULL,
phone VARCHAR(20) NOT NULL,
mail VARCHAR(40) NOT NULL,
groups VARCHAR(20) NOT NULL,
bonus number NOT NULL);

CREATE TABLE mbm_user.orders(
id number PRIMARY KEY,
address VARCHAR(100) NOT NULL,
pay_type VARCHAR(20) NOT NULL,
status VARCHAR(20) NOT NULL,
count number NOT NULL,
client_id number NOT NULL,
FOREIGN KEY (client_id) REFERENCES mbm_user.clients(id));

CREATE TABLE mbm_user.reviews(
id number PRIMARY KEY,
score number NOT NULL,
"comment" VARCHAR(1000),
client_id number NOT NULL,
product_id number NOT NULL,
FOREIGN KEY (client_id) REFERENCES mbm_user.clients(id),
FOREIGN KEY (product_id) REFERENCES mbm_user.products(id));

CREATE TABLE mbm_user.deliveries(
id number PRIMARY KEY,
"date" DATE NOT NULL,
address VARCHAR(200),
type VARCHAR(100),
FOREIGN KEY (id) REFERENCES mbm_user.orders(id));

CREATE TABLE mbm_user.product_photos(
id number PRIMARY KEY,
photo_uri VARCHAR(100) NOT NULL,
product_id number NOT NULL,
FOREIGN KEY(product_id) REFERENCES mbm_user.products(id));

CREATE TABLE mbm_user.products_orders(
product_id number NOT NULL,
order_id number NOT NULL,
FOREIGN KEY(product_id) REFERENCES mbm_user.products(id),
FOREIGN KEY(order_id) REFERENCES mbm_user.orders(id));

CREATE TABLE mbm_user.products_values(
product_id number NOT NULL,
value_id number NOT NULL,
FOREIGN KEY(product_id) REFERENCES mbm_user.products(id),
FOREIGN KEY(value_id) REFERENCES mbm_user.characteristic_values(id));