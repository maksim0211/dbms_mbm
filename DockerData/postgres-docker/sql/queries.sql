SELECT products.name, manufacturers.name, products.model,
characteristics.name, characteristic_values.characteristic_value, characteristics.measument
FROM products_values JOIN products ON product_id = products.id
JOIN manufacturers ON manufacturers.id = products.manufacturer_id
JOIN characteristic_values ON characteristic_values.id = products_values.char_id
JOIN characteristics ON characteristics.id = characteristic_values.char_id;


SELECT
	"characteristics".name, characteristic_values.characteristic_value, char_val_prod_counts.prod_count
FROM "characteristics"
JOIN characteristic_values ON "characteristics".id = characteristic_values.char_id
LEFT JOIN (
	SELECT
		"characteristics".id as char_id, characteristic_values.id as char_val_id, count(*) as prod_count
	FROM products_values
	JOIN characteristic_values ON characteristic_values.id = products_values.char_id
	JOIN "characteristics" ON "characteristics".id = characteristic_values.char_id
	GROUP BY "characteristics".id, characteristic_values.id
) as char_val_prod_counts
	ON char_val_prod_counts.char_val_id = characteristic_values.id
ORDER BY "characteristics".name, characteristic_values.characteristic_value;

SELECT * FROM products
JOIN manufacturers ON manufacturers.id = products.manufacturer_id
WHERE manufacturers.id = 1;






            var qwe = db.CharacteristicValues
                .Select(charValue => new { Value = charValue.CharacteristicValue1, Characteristic = charValue.Char })
                .Join(db.ProductsValues, charValue => charValue.Characteristic.Id, productValue => productValue.CharId,
                    (charValue, productValue) =>
                    new 
                    {
                        Characteristic = charValue.Characteristic.Name,
                        Value = charValue.Value,
                        ProductId = productValue.Product.Id
                    })

                .ToArray();