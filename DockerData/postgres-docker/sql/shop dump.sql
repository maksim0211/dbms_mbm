CREATE TABLE characteristics(
id SERIAL NOT NULL PRIMARY KEY,
name VARCHAR(50) NOT NULL,
description VARCHAR(200) NOT NULL,
image_uri VARCHAR(100) NOT NULL,
measument VARCHAR(20) NOT NULL);

CREATE TABLE characteristic_values(
id SERIAL NOT NULL PRIMARY KEY,
characteristic_value VARCHAR(20) NOT NULL,
char_id INT NOT NULL,
FOREIGN KEY(char_id) REFERENCES characteristics(id));

CREATE TABLE manufacturers(
id SERIAL NOT NULL PRIMARY KEY,
country VARCHAR(40) NOT NULL,
name VARCHAR(50) NOT NULL,
description VARCHAR(200) NOT NULL,
logo_uri VARCHAR(100) NOT NULL);

CREATE TABLE categories(
id SERIAL NOT NULL PRIMARY KEY,
name VARCHAR(40) NOT NULL,
description VARCHAR(100) NOT NULL,
availability BOOLEAN NOT NULL,
image_uri VARCHAR(100) NOT NULL);

CREATE TABLE clients(
id SERIAL NOT NULL PRIMARY KEY,
login VARCHAR(40) NOT NULL,
password VARCHAR(40) NOT NULL,
status VARCHAR(20) NOT NULL,
phone VARCHAR(20) NOT NULL,
mail VARCHAR(40) NOT NULL,
groups VARCHAR(20) NOT NULL,
bonus INT NOT NULL);

CREATE TABLE orders(
id SERIAL NOT NULL PRIMARY KEY,
address VARCHAR(100) NOT NULL,
pay_type VARCHAR(20) NOT NULL,
status VARCHAR(20) NOT NULL,
count SMALLINT NOT NULL,
client_id INT NOT NULL,
FOREIGN KEY (client_id) REFERENCES clients(id));

CREATE TABLE reviews(
id SERIAL NOT NULL PRIMARY KEY,
score SMALLINT NOT NULL,
comment VARCHAR(1000),
client_id INT NOT NULL,
product_id INT NOT NULL,
FOREIGN KEY (client_id) REFERENCES clients(id)
FOREIGN KEY (product_id) REFERENCES products(id));

CREATE TABLE deliveries(
id SERIAL NOT NULL PRIMARY KEY,
date DATE NOT NULL,
address VARCHAR(200),
type VARCHAR(100),
FOREIGN KEY (id) REFERENCES orders(id));

CREATE TABLE products(
id SERIAL NOT NULL PRIMARY KEY,
count SMALLINT NOT NULL,
name VARCHAR(40) NOT NULL,
description VARCHAR(1000) NOT NULL,
cost INT NOT NULL,
availability BOOLEAN NOT NULL,
category_id INT NOT NULL,
manufacturer_id INT NOT NULL,
FOREIGN KEY(category_id) REFERENCES categories(id),
FOREIGN KEY(manufacturer_id) REFERENCES manufacturers(id));

CREATE TABLE product_photos(
id SERIAL NOT NULL PRIMARY KEY,
photo_uri VARCHAR(100) NOT NULL,
product_id INT NOT NULL,
FOREIGN KEY(product_id) REFERENCES products(id));


CREATE TABLE products_orders(
product_id INT NOT NULL,
order_id INT NOT NULL,
FOREIGN KEY(product_id) REFERENCES products(id),
FOREIGN KEY(order_id) REFERENCES orders(id));

CREATE TABLE products_values(
product_id INT NOT NULL,
value_id INT NOT NULL,
FOREIGN KEY(product_id) REFERENCES products(id),
FOREIGN KEY(value_id) REFERENCES characteristic_values(id));
