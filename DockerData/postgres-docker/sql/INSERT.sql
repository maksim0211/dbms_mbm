INSERT INTO categories (name, description, availability, image_uri) VALUES
('Бытовая техника', '-', TRUE, 'none'),
('Смартфоны и гаджеты', '-', TRUE, 'none'),
('ТВ и развлечения', '-', TRUE, 'none'),
('Компьютеры', '-', TRUE, 'none'),
('Офис и сеть', '-', TRUE, 'none'),
('Аксессуары', '-', TRUE, 'none'),
('Ремонт и декор', '-', TRUE, 'none'),
('Автотовары', '-', TRUE, 'none'),
('Инструменты', '-', TRUE, 'none'),
('Красота и здоровье', '-', TRUE, 'none');

INSERT INTO manufacturers(country, name, description, logo_uri) VALUES
('Южная Корея', 'Samsung', 'Samsung Group — южнокорейская группа компаний, один из крупнейших чеболей, основанный в 1938 году. На мировом рынке известен как производитель высокотехнологичных компонентов, включая полноцикловое производство интегральных микросхем, телекоммуникационного оборудования, бытовой техники, аудио- и видеоустройств.', 'none'),
('США', 'Apple', 'Apple — американская корпорация, производитель персональных и планшетных компьютеров, аудиоплееров, смартфонов, программного обеспечения. Один из пионеров в области персональных компьютеров и современных многозадачных операционных систем с графическим интерфейсом.', 'none'),
('Китай', 'Xiaomi', 'Xiaomi Corporation — китайская корпорация, основанная Лэй Цзюнем в 2010 году, занимает третье место в мире и Китае по объёму производства смартфонов.', 'none'),
('Южная Корея', 'LG', 'LG Group — четвёртая по величине южнокорейская финансово-промышленная группа. Основные направления деятельности — электроника, химическая продукция и телекоммуникационное оборудование.', 'none'),
('Тайвань', 'ASUS', 'AsusTek Computer Inc. — расположенная на Тайване транснациональная компания, специализирующаяся на компьютерной электронике (как комплектующие, так и готовые продукты).', 'none'),
('США', 'HP', 'Hewlett-Packard — созданная в 1939 году компания — поставщик аппаратного и программного обеспечения для организаций и индивидуальных потребителей. Штаб-квартира — в Пало-Альто. Одна из крупнейших американских компаний в сфере информационных технологий.', 'none'),
('Тайвань', 'Acer', 'Acer — тайваньская компания по производству компьютерной техники и электроники. Компания занимает 487 место в Fortune Global 500.', 'none'),
('Китай', 'Lenovo', 'Lenovo Group Limited — китайская транснациональная корпорация, выпускающая персональные компьютеры и другую электронику. Является крупнейшим производителем персональных компьютеров в мире с долей на рынке более 20 %, а также занимает пятое место по производству мобильных телефонов.', 'none'),
('Китай', 'Honor', 'Honor — бренд, принадлежащий китайской компании Huawei. В рамках стратегии двойного бренда Huawei Consumer Business Group Honor предоставляет смартфоны, созданные для молодых потребителей, а также ноутбуки, наушники и прочие аксессуары.', 'none'),
('Китай', 'Huawei', 'Huawei Technologies Co. Ltd. — китайская компания, одна из крупнейших мировых компаний в сфере телекоммуникаций. В 1987 году основана бывшим инженером Народно-освободительной армии Китая Жэнь Чжэнфэем.', 'none');

INSERT INTO clients(login, password, status, phone, mail, groups, bonus) VALUES
('000', '000', 'off', '000', '000@mail.com', 'no', 0),
('111', '111', 'off', '111', '111@mail.com', 'no', 0),
('222', '222', 'off', '222', '222@mail.com', 'no', 0),
('333', '333', 'off', '333', '333@mail.com', 'no', 0),
('444', '444', 'off', '444', '444@mail.com', 'no', 0),
('555', '555', 'off', '555', '555@mail.com', 'no', 0),
('666', '666', 'off', '666', '666@mail.com', 'no', 0),
('777', '777', 'off', '777', '777@mail.com', 'no', 0),
('888', '888', 'off', '888', '888@mail.com', 'no', 0),
('999', '999', 'off', '999', '999@mail.com', 'no', 0);

INSERT INTO products(count, name, description, cost, category_id, manufacturer_id, model) VALUES
(213, 'Стиральная машина', 'Стиральная машина оснащена электроприводом, что позволяет существенно сократить траты электроэнергии и воды, а также уменьшить шумовой эффект от работы до минимума. При небольших габаритах эта машинка позволяет погрузить до 4 кг белья. Энергосбережение было классифицировано, европейской организацией стандартов, оценкой «А», такой же оценкой отмечена эффективность стирки.', '11000', 1, 4, 'IWUD 4085'),
(94, 'Смартфон', 'Смартфон сочетает эффектное цветовое исполнение, высокий уровень производительности и широкие функциональные возможности. Аппаратная платформа с процессором MediaTek и 1 ГБ оперативной памяти обеспечивают быструю работу приложений, веб-страниц, мультимедиа. Под хранение данных пользователя выделено 16 ГБ памяти.', 6000, 2, 1, 'Galaxy A01 Core'),
(124, 'Смартфон', 'Каждый элемент и каждая опция в смартфоне гарантируют удобство использования. Этот смартфон приятно держать в руке, алюминиевый корпус серого цвета придает смартфону приятную тяжесть и защищает от царапин. Дисплей в 4.7 дюйма с разрешением 1334x750 пикселей и плотностью 326 ppi обеспечит высококачественную картинку. Экран защищен от царапин и имеет олеофобное напыление, поэтому он будет меньше пачкаться. Памяти в 32 ГБ будет достаточно для работы и яркого отдыха.', 25000, 2, 2, 'iPhone 6S'),
(943, 'Смартфон', 'Смартфон корпусе из пластика объединил универсальные решения, которые понравятся каждому пользователю. В первую очередь необходимо отметить его достойное быстродействие, благодаря которому устройство легко работает в десятке приложений одновременно. В основе его функционирования – процессор MediaTek Helio G25, ускоритель PowerVR GE8320 и сразу 2 ГБ оперативной памяти.', 9000, 2, 3, ' Redmi 9A'),
(122, 'Ноутбук', 'Быстродействующий высокопроизводительный ноутбук в стильном и легком корпусе создан для длительной автономной работы. Эта надежная модель с диагональю 15,6 дюйма идеально подходит для повседневной работы, гарантирует идеальное качество звука и предоставляет быстрые и безопасные варианты хранения данных.', 23000, 4, 8, 'IdeaPad S145'),
(19, 'Wi-Fi роутер', 'Маршрутизатор подходит для дома и небольшого офиса. Он обеспечивает быструю скорость передачи данных. С ним можно смотреть фильмы в качестве HD, играть по сети, искать необходимую информацию в сети или делиться новостями с друзьями.', 1500, 5, 5, 'RT-N11P'),
(155, 'Ноутбук', 'С ноутбуком любая дорога будет веселей и ярче. Ведь с ним не придется скучать. На 11.6-дюймовой диагонали дисплея типа TN+film с HD-разрешением 1366x768 пикселей вы сможете сполна наслаждаться мобильным просмотром видеотреков и фильмов. А матовое покрытие при этом минимизирует возможные блики. Производительный процессор N4120 линейки Celeron с 4 ядрами в активе и частотным диапазоном от 1.1 до 2.6 МГц позволит без проблем и задержек загружать самые разные по объему приложения. А стереодинамики позволят получать удовольствие от любимых аудиотреков.', 17500, 4, 7, 'TravelMate B1 TMB118-M-C0EA'),
(334, 'Ноутбук', 'Тонкий и компактный ноутбук HP с экраном 15,6" (39,6 см) с тончайшими рамками и длительным временем работы от аккумулятора позволяет сохранять высокую продуктивность и проводить время с удовольствием, где бы вы ни находились. Процессор Intel и надежный флеш-накопитель помогут справиться с самыми сложными задачами. Высокая продуктивность в течение всего дня благодаря длительному времени работы от аккумулятора и сверхточному тачпаду.', 2500, 4, 6, 'Laptop 15s-fq0055ur'),
(17, 'Портативная колонка', 'Портативная колонка Huawei CM510 представлена 2 динамиками в симпатичных зеленых корпусах. Они изготовлены из металла и защищены от влаги. Их совокупная выходная мощность составляет 3 Вт. Благодаря небольшим размерам колонки можно брать с собой куда угодно, а удобный ремешок делает их удобными для переноски. Они будут сопровождать вас в ремонте, в уборке и даже на тренировках по воркауту!', 1500, 3, 10, 'CM510'),
(23, 'Весы', 'Весы позволяет за одно измерение сформировать отчет о физическом состоянии человека. Это диагностические весы, в которых реализованы передовые технологии и которые обладают высокой точностью измерений. Выполнены электронные весы в белом корпусе в виде платформы, изготовленной из закаленного стекла. Продуманный дизайн обусловливает удобство процесса измерения: модель оснащена датчиком давления с чипом BIA 4 и регулируемыми ножками.', 1500, 10, 9, 'AH100 Smart Scale');

INSERT INTO reviews(score, comment, client_id, product_id) VALUES
(1, 4, 'Вааау!', 1, 1),
(2, 5, 'Бомба!(ДМБ)', 2, 2),
(3, 3, 'Так себе...', 3, 3),
(4, 5, 'Топ за свои деньги', 4, 4),
(5, 5, 'Майнкрафт тянет на ультрах', 5, 5),
(6, 5, 'Сидим на компах с пацанами', 5, 6),
(7, 5, 'Восхитительно!', 5, 7),
(8, 5, 'Вот это да!', 6, 8),
(9, 5, 'Теперь все соседи слушают OG Loc', 7, 9),
(10, 5, 'Вешу не больше тонны', 8, 10);

INSERT INTO orders(id, address, pay_type, status, count, client_id) VALUES
(1, 'Адрес1', 'Наличная', 'В пути', 1, 1),
(2, 'Адрес2', 'Безналичная', 'Ожидается подтверждение', 3, 2),
(3, 'Адрес3', 'Наличная', 'Отменен', 1, 3),
(4, 'Адрес4', 'Наличная', 'В пути', 1, 4),
(5, 'Адрес5', 'Безналичная', 'В пути', 1, 5),
(6, 'Адрес6', 'Безналичная', 'В пути', 1, 6),
(7, 'Адрес7', 'Наличная', 'В пути', 1, 7),
(8, 'Адрес8', 'Наличная', 'В пути', 1, 8),
(9, 'Адрес9', 'Безналичная', 'В пути', 2, 9),
(10, 'Адрес10', 'Наличная', 'В пути', 1, 10);

INSERT INTO products_orders(product_id, order_id) VALUES
(1, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 9),
(10, 10);

INSERT INTO characteristics(name, description, image_uri, measument) VALUES
('Объем встроенной памяти', '-', 'none', 'Гб'),
('Объем оперативной памяти', '-', 'none', 'Гб'),
('Емкость аккумулятора', '-', 'none', 'мА*ч'),
('Диагональ экрана', '-', 'none', 'Дюйм'),
('Количество ядер', '-', 'none', '-'),
('Количество сим-карт', '-', 'none', '-'),
('Линейка процессора', '-', 'none', '-'),
('Общий объем накопителя', '-', 'none', '-'),
('Тип накопителя', '-', 'none', '-'),
('Объем видеопамяти', '-', 'none', '-');

INSERT INTO characteristic_values(characteristic_value, char_id) VALUES
('8', 1), ('16', 1), ('32', 1), ('64', 1), ('128', 1), ('256', 1),
('2', 2), ('4', 2), ('8', 2), ('16', 2), ('32', 2),
('2000', 3), ('3000', 3), ('4000', 3), ('5000', 3), ('6000', 3),('8000', 3), 
('4', 4), ('4.5', 4), ('5', 4), ('6', 4), ('8', 4), 
('1', 5), ('2', 5), ('4', 5), ('6', 5), ('8', 5), 
('1', 6), ('2', 6),
('Core i3', 7), ('Core i5', 7), ('Core i7', 7), ('Core i9', 7), 
('128', 8), ('256', 8), ('512', 8),  ('1024', 8),
('SSD', 9), ('HDD', 9),
('1', 10), ('2', 10), ('4', 10), ('6', 10), ('8', 10);

INSERT INTO products_values(product_id, char_id) VALUES
(2, 1), (2, 13), (2, 20), (2, 24), (2, 28), (2, 41),
(3, 2), (3, 14), (3, 21), (3, 24), (3, 29), (3, 41);

